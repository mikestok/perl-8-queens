Source code from the *A Subjective Look at Object-Oriented
Programming* article from Volume 1, Issue 4 of **The Perl Journal**
from 1996.

The article has been recreated
[on my website](http://www.stok.ca/~mike/subjective-look/), and
this is the code I used to check that it produces the same results
today.

Should work with any Perl 5, :

```
$ perl -v

This is perl 5, version 30, subversion 0 (v5.30.0) built for darwin-thread-multi-2level

Copyright 1987-2019, Larry Wall

Perl may be copied only under the terms of either the Artistic License or the
GNU General Public License, which may be found in the Perl 5 source kit.

Complete documentation for Perl, including FAQ lists, should be found on
this system using "man perl" or "perldoc perl".  If you have access to the
Internet, point your browser at http://www.perl.org/, the Perl Home Page.

$ ./main
row: 1 column: 1
row: 5 column: 2
row: 8 column: 3
row: 6 column: 4
row: 3 column: 5
row: 7 column: 6
row: 2 column: 7
row: 4 column: 8
```
