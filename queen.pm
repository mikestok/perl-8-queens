package Queen;

sub new {
  my $type = shift;
  return bless {}, $type;
}

sub initialColumn {
  my $self = shift;
  $self->{column} = shift;
  $self->{neighbor} = shift;
  return;
}

sub first {
  my $self = shift;

  $self->{row} = 1;

  if ($self->{neighbor}->first()) {
    return $self->testOrAdvance();
  }
  return 0;
}

sub testOrAdvance {
  my $self = shift;

  if ($self->{neighbor}->canAttack(@{$self}{'row', 'column'})) {
    return $self->next();
  }
  return 1;
}

sub canAttack {
  my $self = shift;
  my $row = shift;
  my $column = shift;

  $self->{row} == $row and return 1;

  # $cd and $rd contain the column and row
  # differences between the position and this queen.
  # If $cd == $rd they're on the same diagonal.

  my $cd = abs($column - $self->{column});
  my $rd = abs($row - $self->{row});

  $cd == $rd and return 1;

  return $self->{neighbor}->canAttack($row, $column);
}

sub next {
  my $self = shift;
  local $^W = 0; # turn warnings off

  if ($self->{row} == 8) {
    unless ($self->{neighbor}->next()) { return 0; }
    else { $self->{row} = 0 }
  }
  $self->{row}++;
  return $self->testOrAdvance();
}

sub print {
  my $self = shift;

  $self->{neighbor}->print();

  print "row: $self->{row} column: $self->{column}\n";
  return;
}

1;
