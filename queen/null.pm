package Queen::Null;

@ISA = qw(Queen);

sub canAttack { return 0; }
sub first { return 1; }
sub next { return 0; }

sub print { return; }

1;
